/*
 * exercise05.c
 *
 *  Created on: 15 Sep 2019
 *      Author: mariasaveleva
 */

#include <stdio.h>

#define MATRIX_COLUMNS 3
#define MATRIX_ROWS 3
#define VECTOR_ROWS 3

void multiply(int result[VECTOR_ROWS], const int matrixA[MATRIX_COLUMNS][MATRIX_ROWS], const int vector[VECTOR_ROWS]);
void printMatrix(int matrix[MATRIX_COLUMNS][MATRIX_ROWS]);
void printVector(int vector[VECTOR_ROWS]);

void startExercise05() {
	printf("Enter 3x3 matrix. Press \"enter\" to separate values.\n");
	int matrix[MATRIX_COLUMNS][MATRIX_ROWS];
	for (int i = 0; i < MATRIX_COLUMNS; i++) {
		for (int j = 0; j < 3; j++) {
			int value = 0;
			scanf("%d", &value);
			matrix[i][j] = value;
		}
	}

	printf("Enter 3x1 vector. Press \"enter\" to separate values.\n");
	int vector[VECTOR_ROWS];
	for (int i = 0; i < VECTOR_ROWS; i++) {
		int value = 0;
		scanf("%d", &value);
		vector[i] = value;
	}

	printMatrix(matrix);
	printVector(vector);

	int result[VECTOR_ROWS];
	multiply(result, matrix, vector);

	printf("Result: \n");
	printVector(result);
}

void multiply(int result[VECTOR_ROWS], const int matrixA[MATRIX_COLUMNS][MATRIX_ROWS], const int vector[VECTOR_ROWS]) {
   for (int i = 0; i < MATRIX_COLUMNS; i++) {
	   int value = 0;
	   for (int j = 0; j < MATRIX_ROWS; j++) {
		   value += matrixA[i][j] * vector[j];
	   }
	   result[i] = value;
   }
}

void printMatrix(int matrix[MATRIX_COLUMNS][MATRIX_ROWS]) {
	for (int i = 0; i < MATRIX_COLUMNS; i++) {
		for (int j = 0; j < MATRIX_ROWS; j++) {
			printf("%d ", matrix[i][j]);
		}
		printf("\n");
	}
}

void printVector(int vector[VECTOR_ROWS]) {
	for (int i = 0; i < VECTOR_ROWS; i++) {
		printf("%d", vector[i]);
		printf("\n");
	}
}
