/*
 * exercise03.c
 *
 *  Created on: 29 Aug 2019
 *      Author: mariasaveleva
 */

#include <stdio.h>

float calculateAverage(int *array, int length);
void startGradesProgram();

void startExercise03() {
	startGradesProgram();
}

float calculateAverage(int *array, int length) {
    int sum = 0.0f;
    for (int i = 0; i < length; i++) {
        sum += array[i];
    }

    return (float)sum / (float)length;
}

void startGradesProgram() {
    int numberOfStudents = 0;

    printf("Enter number of students:\n");
    scanf("%d", &numberOfStudents);

    int grades[numberOfStudents];
    for (int i = 0; i < numberOfStudents; i++) {
    	int studentNumber = i + 45000;
    	printf("Enter grade for student with number %d.\n", studentNumber);

    	int grade = -1;
    	do {
    		scanf("%d", &grade);

    		if (grade < 0 || grade > 5) {
    			printf("Please enter valid grade in range 0-5.\n");
    		}
    	} while (grade < 0 || grade > 5);

    	grades[i] = grade;
    }

    float averageGrade = calculateAverage(grades, numberOfStudents);
    printf("Average grade: %.2f", averageGrade);
}
