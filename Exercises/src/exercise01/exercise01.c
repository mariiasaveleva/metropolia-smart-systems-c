/*
 * exercise01.c
 *
 *  Created on: 29 Aug 2019
 *      Author: mariasaveleva
 */

#include <stdio.h>


void startExercise01() {
	printf("Average value calculator started.\n");
	printf("Enter positive values one by one.\n");
	printf("Enter '0' to calculate the result\n");

	int userInput = -1;
	int numberOfValues = 0;
	int sum = 0;

	do {
		scanf("%d", &userInput);

		if (userInput < 0) {
			printf("Please enter only positive values. Other values will be ignored.\n");
		} else if (userInput == 0) {
			if (numberOfValues == 0) {
				printf("Unable to calculate average for zero input values.\n");
			} else {
				float result = (float)sum / (float)numberOfValues;
				printf("The result is: %.3f.\n", result);
			}
		} else {
			sum += userInput;
			numberOfValues++;
		}
	} while (userInput != 0);
}

