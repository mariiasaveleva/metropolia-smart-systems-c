/*
 * exercise04.c
 *
 *  Created on: 2 Sep 2019
 *      Author: mariasaveleva
 */


#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define MAX_NUMBER_OF_HOURS 23
#define MAX_NUMBER_OF_MINUTES 59

bool isValidTime(int hours, int minutes);
int calculateHours(int currentHours, int sleepHouts);
int calculateMinutes(int currentMinutes, int sleepMinutes);

void startExercise04() {
	int currentHours = 0;
	int currentMinutes = 0;
	bool isCurrentTimeValid = false;

	while (isCurrentTimeValid == false) {
		printf("Enter current time (hh:mm): ");
		scanf("%d:%d", &currentHours, &currentMinutes);
		printf("Hours: %d, minutes: %d\n", currentHours, currentMinutes);

		isCurrentTimeValid = isValidTime(currentHours, currentMinutes);
		if (!isCurrentTimeValid) {
			printf("Invalid current time. Please enter correct time in 24-hours format.\n");
		}
	}

	int sleepHours = 0;
	int sleepMinutes = 0;
	bool isSleepTimeValid = false;
	while (isSleepTimeValid == false) {
		printf("How long do you want to sleep (h:mm): ");
		scanf("%d:%d", &sleepHours, &sleepMinutes);
		printf("Hours: %d, minutes: %d\n", sleepHours, sleepMinutes);

		isSleepTimeValid = isValidTime(sleepHours, sleepMinutes);
		if (!isSleepTimeValid) {
			printf("Invalid current time. Please enter correct time in 24-hours format.\n");
		}
	}

	int resultHours = calculateHours(currentHours, sleepHours);
	int resultMinutes = calculateMinutes(currentMinutes, sleepMinutes);
	printf("If you go to bed now you must wake up at %d:%d\n", resultHours, resultMinutes);
}

bool isValidTime(int hours, int minutes) {
	bool hoursAreValid = hours >= 0 && hours <= MAX_NUMBER_OF_HOURS;
	bool minutesAreValid = minutes >= 0 && minutes <= MAX_NUMBER_OF_MINUTES;

	return hoursAreValid && minutesAreValid;
}

int calculateHours(int currentHours, int sleepHours) {
	int resultHours = currentHours + sleepHours;
	if (resultHours > MAX_NUMBER_OF_HOURS) {
		resultHours = sleepHours - (MAX_NUMBER_OF_HOURS - currentHours);
	}

	return resultHours;
}

int calculateMinutes(int currentMinutes, int sleepMinutes) {
	int resultMinutes = currentMinutes + sleepMinutes;
	if (resultMinutes > MAX_NUMBER_OF_MINUTES) {
		resultMinutes = sleepMinutes - (MAX_NUMBER_OF_MINUTES - currentMinutes);
	}

	return resultMinutes;
}
