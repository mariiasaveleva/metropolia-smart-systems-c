/*
 * exercise02.c
 *
 *  Created on: 29 Aug 2019
 *      Author: mariasaveleva
 */

#include <stdio.h>

void startExercise02() {
	float numbers[12];
	float income;

	printf("Enter the yearly income:\n");
	scanf("%f", &income);
	for(int i = 0; i < 12; i++) {
		numbers[i] = income / 12.5f;
	}
	numbers[6] = numbers[6] * 1.5f;

	for(int i = 0; i < 12; i++) {
		printf("element[%2i] = %11.5f\n", i, numbers[i]);
	}

	printf("total:      = %11.5f\n", income);
}
